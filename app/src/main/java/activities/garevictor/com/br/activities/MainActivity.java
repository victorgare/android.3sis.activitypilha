package activities.garevictor.com.br.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText edtEmail;
    private EditText edtSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtSenha = (EditText) findViewById(R.id.edtPassword);
    }

    public void login(View v){
        Intent i = new Intent(this, ValidaLoginActivity.class);

        //criara a view em um novo espaço de memoria
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        i.putExtra("email", edtEmail.getText().toString());
        i.putExtra("senha", edtSenha.getText().toString());

        startActivity(i);

    }
}
