package activities.garevictor.com.br.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class ValidaLoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_valida_login2);

        Bundle params = getIntent().getExtras();

        String email = params.getString("email");
        String senha = params.getString("senha");

        if(email.equalsIgnoreCase("SI")&& senha.equalsIgnoreCase("123")){
            Toast.makeText(this, "Válido", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Inválido", Toast.LENGTH_SHORT).show();
        }
    }
}
